# Company managament tool

Welcome to my final project of the computer science course taught in the first year of the dregree in biomedical engineering.
The project has been fully developed in Java. It is an easy-going managament tool for a biomedical company as explained below.
_Please, note that the comments on codes are for learning purposes_.

## [Log-in](https://bitbucket.org/leccavi/java-project/src/master/src/Clases/Login.java)
The first screen is the user identification to access the functionalities assigned according to the role. At the moment, there are 
only two predefined users, a manager director and an employee:

- **Manager**: maescl@gerente.com (user) and 13579 (password)

- **Employee**: lealecvi@admin.com (user) and 08642 (password)

If the log-in fails, a window will warn you about the problem.

![picture](screenshots/login.png)

## [Main menu](https://bitbucket.org/leccavi/java-project/src/master/src/Clases/MenuGerente.java)
This screen shows the manager director's funcitonalities. The main one is employee management.
![picture](screenshots/main.png)


## [Managament tool](https://bitbucket.org/leccavi/java-project/src/master/src/Clases/addTb.java)
Here it is depicted the company database (connected to a SQL Server). The manager director can add or delete employees.
![picture](screenshots/database.png)

![picture](screenshots/dbsql.png)


## [Chat](https://bitbucket.org/leccavi/java-project/src/master/src/Clases/JChat.java)
Finally, there is a chat app. It must be specified a nickname and the IP address to which you wish to communicate.
![picture](screenshots/chat.png) 