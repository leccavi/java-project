/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;

import java.awt.*;
import java.io.IOException;
import java.net.*;
import java.util.Scanner;
import javax.swing.*;
import java.awt.event.*;
import javax.swing.JTextField;
import static javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER;

/**
 *
 * @author LeandroLV
 */
public class JChat extends JFrame{
    
    private JLabel tituloChat;
    private JTextArea areaChat;
    private JScrollPane deslizador;
    private JTextField mensaje;
    private JButton enviar;
    
    //ATRIBUTOS PARA EL FUNCIONAMIENTO DEL CHAT
    public final int PUERTO_CHAT=2014;
    public InetAddress direccionIPLocal;
    public InetAddress direccionIPRemota;
    public DatagramSocket socketChat;
    public String usuario;
   
    
    /**
     * JChat.
     */
    public JChat(){
        setSize(667,600);
        setUndecorated(false);
        iniciar();
        setTitle("Chat informática y redes");
        initComponents();
        setVisible(true);   
    }
    
    
    
    /**
     * initComponents.
     */
    private void initComponents(){
        
        getContentPane().setLayout(null);
        setResizable(false);
        
        // TITULO CHAT //
        tituloChat = new JLabel();
        tituloChat.setText("BioChat");
        tituloChat.setForeground(Color.BLACK);
        tituloChat.setFont(new Font("Orbitron", 1, 30));
        getContentPane().add(tituloChat);
        tituloChat.setBounds(267, 28, 204, 30);
        
        
        // ÁREA DE CHAT //
        areaChat = new JTextArea();
        areaChat.setLineWrap(true);
        areaChat.setWrapStyleWord(true); 
        areaChat.setEditable(false);
        areaChat.setBackground(Color.DARK_GRAY);
        deslizador=new JScrollPane(areaChat);
        areaChat.setFont(new Font("Fenix", 0, 16));
        getContentPane().add(deslizador);
        deslizador.setBounds(20, 76, 625, 400);
        
        
        // CAMPO MENSAJE //
        mensaje = new JTextField();
        mensaje.setBackground(Color.DARK_GRAY);
        mensaje.setForeground(Color.WHITE);
        mensaje.setText("");
        mensaje.setFont(new java.awt.Font("Fenix", 0, 16));
        getContentPane().add(mensaje);
        mensaje.setBounds(17, 490, 540, 30);
        mensaje.addKeyListener(new java.awt.event.KeyAdapter() {
            public void KeyPressed(java.awt.event.KeyEvent evt){
                mensajeKeyPressed(evt);
            }
        });
        
        
        // BOTÓN ENVIAR //
        enviar = new JButton();
        enviar.setText("Enviar");
        enviar.setToolTipText("Pulse para mandar el mensaje");
        enviar.setFont(new Font("Fenix", 0, 16));
        getContentPane().add(enviar);
        enviar.setBounds(571, 493, 80, 25); 
        enviar.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent evt) {
                String mensajeIntroducido;
                mensajeIntroducido=mensaje.getText();
                mensaje.setText("");
                areaChat.setForeground(Color.GREEN);
                areaChat.append( usuario + " >> " + mensajeIntroducido + "\n");
                enviarMensaje(usuario + "\n" + mensajeIntroducido);
            }
        });

        
        // BACKGROUND //
        PanelImagen p= new PanelImagen("/Imagenes/Splash.jpg");
        getContentPane().add(p);
        p.setBounds(0, 0, 1000, 600);      
    }
    
    
    
    /**
     * iniciar.
     */
    public void iniciar(){
        setLocationRelativeTo(null);
    }
    
    
    /**
     * conectar.
     */
    public void conectar(){
        InetAddress miDireccionIP;
        try{
        usuario = JOptionPane.showInputDialog("Introduzca nick");
            //ahora obtenemos la direccionIP de nuestra máquina
            miDireccionIP = InetAddress.getLocalHost();
        
            System.out.println("La máquina: "+miDireccionIP.getHostName()
                                + " tiene dirección IP: "
                                +miDireccionIP.getHostAddress());
            
            direccionIPLocal = miDireccionIP;
            
            String IP = JOptionPane.showInputDialog("Introduzca IP destino");
            System.out.println("La direccion IP introducida es: " +IP);
            direccionIPRemota = InetAddress.getByName(IP);
            
            socketChat = new DatagramSocket(PUERTO_CHAT);
            //titulo =("LOCAL ("+direccionIPLocal+") <-----> REMOTO 
            // ("+direccionIPRemota+")");

        }catch (UnknownHostException uhe){
            System.out.println("La dirección no ha podido ser resuelta");
        
        } catch (SocketException se) {
            System.out.println("Error al crear el socket");
        } 
    }  
    
    
    /**
     * enviarMensaje.
     * @param mensaje 
     */
    public void enviarMensaje(String mensaje){
        byte[] bufferEnvio;
        
        try {
        
        bufferEnvio = mensaje.getBytes();
        
        DatagramPacket paqueteEnvio = new DatagramPacket(bufferEnvio, 
                bufferEnvio.length, direccionIPRemota, PUERTO_CHAT);
        
        socketChat.send(paqueteEnvio);
        
        } catch (IOException ioe) {
            System.out.println("Error en la entrada/salida E/S");
        }
    }
    
    
    /**
     * leerMensaje.
     * @return 
     */
    public String leerMensaje() {
        String mensajeIntroducido = mensaje.getText();
        mensaje.setText("");
        return mensajeIntroducido;
    }
    
    
    /**
     * visualizarMensajeSaliente.
     * @param mensaje 
     */
    public void visualizaMensajeSaliente(String mensaje){   
        areaChat.append( usuario + ":\n " + mensaje + "\n");  
    }
    
    
    /**
     * visualizarMensajeEntrante.
     * @param texto 
     */
    public void visualizaMensajeEntrante(String texto){
        areaChat.setForeground(Color.magenta);
        areaChat.append( texto + "\n");
    }
    
    
    /**
     * arrancaReceptor.
     */
    public void arrancaReceptor(){
        ReceptorMensajes receptor = new ReceptorMensajes();
        receptor.start();
        receptor.recibirVentana(this);
    }
    
    /**
     * trataEnvio.
     * @param evt 
     */
    private void trataEnvio(ActionEvent evt){
        String mensajeIntroducido;
        mensajeIntroducido=mensaje.getText();
        mensaje.setText("");
        areaChat.setForeground(Color.GREEN);
        areaChat.append( usuario + " : " + mensajeIntroducido + "\n");
    }
    
    // INTERCEPCION DE LA TECLA
    private void mensajeKeyPressed(java.awt.event.KeyEvent evt){
      
       if (evt.getKeyCode()==java.awt.event.KeyEvent.VK_ENTER){ 
       
        }
    }


class ReceptorMensajes extends Thread {
        
        private JChat ventanaPrincipal;
        
        public void recibirVentana(JChat object) {
            ventanaPrincipal = object;

        }
        
        @Override
        public void run () {
            
            byte [] bytesRecibidos = new byte [256];
            int longitud;
            InetAddress dirIP;
            String mensaje;
            
            try {
            
            DatagramPacket paqueteRecibido;

            while (true) {
                paqueteRecibido = new DatagramPacket(bytesRecibidos, 256);
                ventanaPrincipal.socketChat.receive(paqueteRecibido);


                dirIP = paqueteRecibido.getAddress();
                longitud = paqueteRecibido.getLength();
                
                mensaje = new String (bytesRecibidos,0,longitud);
                
                System.err.println("Servidor>>> recibido el mensaje: "+mensaje
                                    +" desde: "+dirIP);
                ventanaPrincipal.visualizaMensajeEntrante(dirIP + 
                        " << "+mensaje);
            }
                
            } catch (IOException ioe) {
            System.out.println(ioe.toString());
            }
        }
    }       
}

