/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;


import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import sun.security.util.Password;



/**
 *
 * @author LeandroLV
 */
public class Login extends JFrame{
    
    // COMPONENTES QUE USAREMOS
    private JLabel TituloP;
    private JLabel usuario;
    private JLabel contraseña;
    private JButton aceptar;
    private static JTextField CampoU;
    private static JPasswordField CampoC; // campo oculto
    private JButton info;
    private ImageIcon miImagen;
    
    
    /**
     * Login. Método constructor de la clase Login.
     */
    public Login(){
        setSize(900,290); // tamaño de la ventana
        iniciar(); // método inciar
        setTitle("BIOMED V.1"); // titulo de la ventana
        initComponents(); //iniciar los controles o componentes
        setVisible(true);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); 
    }
    
    /**
     * initComponents. Método para iniciar y situar los componentes en la 
     * ventana.
     */
    private void initComponents(){
        
        // Layout(null): esto nos permetirá utilizar el método .setBounds para
        // poder situar los componentes donde queramos.
        getContentPane().setLayout(null);
        // Quitamos a la ventana las capacidad de estirarse y maximizarse
        setResizable(false);
        
        // PROGRAMA TITULO
        TituloP = new JLabel();
        // Añadimos el titulo
        TituloP.setText("BIOMED V.1");
        // Lo centramos dentro de su ancho
        TituloP.setHorizontalAlignment(SwingConstants.CENTER);
        // Añadimos la fuente al titulo
        TituloP.setFont(new Font("Orbitron", 5, 18));
        // Añadimos el titulo a la ventana
        getContentPane().add(TituloP);
        // Como consecuencia del setLayout(null), podemos utilizar el método
        // set bounds... (eje X, eje Y, ancho del componente, vertical del 
        // componente).
        TituloP.setBounds(350, 36, 204, 20);
        
        
        // ** DE FORMA ANÁLOGA PARA EL RESTO DE COMPONENTES ** //
        
        
        // ERIQUETA USUARIO //
        usuario = new JLabel();
        usuario.setText("Usuario:");
        usuario.setFont(new Font("Helvetica", 0, 14));
        getContentPane().add(usuario);
        usuario.setBounds(325, 100, 100, 30);
        
        // ETIQUETA CONTRASEÑA //
        contraseña = new JLabel();
        contraseña.setText("Contraseña:");
        contraseña.setFont(new Font("Helvetica", 0, 14));
        getContentPane().add(contraseña);
        contraseña.setBounds(325, 140, 100, 30);
        
        
        // CAMPO USUARIO //
        CampoU = new JTextField();
        CampoU.setText("");
        getContentPane().add(CampoU);
        CampoU.setFont(new Font("Helvetica", 0, 12));
        CampoU.setBounds(420, 100, 200, 30);
        // AÑADIMOS UN ESCUCHADOR A CampoU DE TIPO TECLADO, ES DECIR, UN
        // ESCUCHADOR QUE ESTÉ A LA ESPERA DE RECIBIR UN EVENTO DE TIPO TECLADO
        // Y DE ESTA MANERA QUE PUEDA TRATAR DICHO EVENTO MEDIANTE
        // EL CORRESPONDIENTE MÉTODO QUE HEMOS DEFINIDO (MÉTODO QUE HARÁ LO QUE
        // NOSOTROS QUERAMOS).
        CampoU.addKeyListener(new KeyAdapter(){
        @Override
        public void keyPressed(KeyEvent evt){
            // Método que tratará el evento
            entrarTecla(evt);      
        }
    });
        
        CampoC = new JPasswordField();
        CampoC.setText("");
        getContentPane().add(CampoC);
        CampoC.setFont(new java.awt.Font("Helvetica", 0, 12));
        CampoC.setBounds(420, 140, 200, 30);
        CampoC.addKeyListener(new KeyAdapter(){
        @Override
        public void keyPressed(KeyEvent evt){
            entrarTecla(evt);    
        }
    });
        
        // BOTON DE INICIO //
        aceptar = new JButton();
        aceptar.setToolTipText("Pulse para entrar");
        aceptar.setText("Aceptar");
        getContentPane().add(aceptar);
        aceptar.setFont(new java.awt.Font("Helvetica", 0, 12));
        aceptar.setBounds(420, 175, 90, 30);
        // AÑADIMOS AL BOTÓN UN ESCUCHADOR DE EVENTO (QUE SE ENCARGARÁ
        // DE ANALIZAR QUÉ EVENTO SE HA PRODUCIDO) Y LO CREAMOS...
        // CONCRETAMENTE, ACTIONLISTENER PERMITE A UN COMPONENTE
        // (ADDTB) RESPONDER A LAS ACCIONES QUE OCURREN EN ÉL (EVENTOS DE TIPO
        // ACTION EVENT), COMO UN CLICK SOBRE UN BOTÓN
        aceptar.addActionListener(new ActionListener() {   
            @Override
            public void actionPerformed(ActionEvent evt) {
                // ASOCIAMOS EL MÉTODO QUE SE ENCARGARÁ DE TRATAR EL EVENTO, O
                // QUE ES LO MISMO: QUE HAGA LO QUE QUERAMOS AL PRODUCIRSE EL
                // EVENTO CAPTURADO POR EL ESCUCHADOR.
                aceptarActionPerformed(evt);    
            }
        });
        
        aceptar.addKeyListener(new KeyAdapter(){
        @Override
        public void keyPressed(KeyEvent evt){
            entrarTecla(evt);    
        }
    });
                
        
        // BOTÓN INFORMACIÓN //
        // Cargamos el icono creando un objeto de ImageIcon y pasándole como
        // parámetro el nombre del icono que se encuentra dentro de la carpeta
        // de este proyecto
        miImagen = new ImageIcon(getClass().getResource("/icons/info.png"));
        // Creamos el botón pasángole la imagen para que lo asocie
        info = new JButton(miImagen);
        // Quitamos el borde del boton
        info.setBorderPainted(false);
        // Por cuestión de estética, ponemos al boton un fondo de color blanco.
        // En WINDOWS se ve el fondo, sin embargo, en la versión MAC, no 
        // aparece, lo que hace, aún más, bonita la app.
        info.setBackground(Color.WHITE);
        // Ayuda que aparecerá cuando el ratón se situe sobre el botón durante
        // un corto tiempo
        info.setToolTipText("Acerca del programa");
        getContentPane().add(info);
        info.setBounds(490, 175, 90, 30); 
        info.addActionListener(new ActionListener() {  
            @Override
            public void actionPerformed(ActionEvent evt){
                infoActionPerformed(evt);
            }
        });
       
        // BACKGROUND //
        // Creamos una clase de PanelImagen pasándole como parámetro donde se 
        // ecuentra la imagen de fondo dentro de la carpeta de este proyecto
        PanelImagen p= new PanelImagen("/Imagenes/dna.jpg");
        getContentPane().add(p);
        p.setBounds(0, 0, 900, 290);      
        
    }
    /**
     * iniciar. Método para centrar la ventana en la pantalla.
     */
    public void iniciar(){
        setLocationRelativeTo(null);
    }
    
    
  
    /**
     * aceptarActionPerformed.
     * @param evt 
     */
    private void  aceptarActionPerformed(ActionEvent evt) {

        // DEFINIMOS DOS USUARIOS TIPO, CON SUS NICKS Y PASSWORDS //
        
        String Usuario="maescl@gerente.com";
        String Contraseña="13579";
        
        String Usuario2="lealecvi@admin.com";
        String Contraseña2="08642";
        
        // OBTENEMOS LOS DATOS OCULTOS DE LOS CAMPOS CONTRASEÑA //
        String Pass=new String(CampoC.getPassword());
        String Pass2=new String(CampoC.getPassword());
        
        
        //  SI EL CAMPO USUARIO ES IGUAL AL STRING URUARIO Y, ADEMÁS, TAMBIÉN
        // EL CAMPO CONTRASEÑA ES IGUAL AL STRING CONTRASEÑA
        if(CampoU.getText().equals(Usuario) && Pass.equals(Contraseña)) {
            // Creamos el ScreenSplash
            ScreenSplash SSA = new ScreenSplash();
            // Activamos el método del gerente para que aparezca su MENÚ
            // * VER CLASE SCRENSPLASH *
            SSA.activarGerente();
            // Nos deshacemos de la ventana Login
            dispose();        
        }
        
        // SINO SI EL CAMPO USUARIO ES IGUAL AL STRING URUARIO2 Y, ADEMÁS, 
        // TAMBIÉN EL CAMPO CONTRASEÑA ES IGUAL AL STRING CONTRASEÑA2
        else if(CampoU.getText().equals(Usuario2) && Pass2.equals(Contraseña2)){
            // Creamos el ScreenSplash
            ScreenSplash SSA = new ScreenSplash();
            // Activamos el método del admin para que aparezca su MENÚ
            // * VER CLASE SCREENSPLASH *
            SSA.activarAdmin();
            // Nos deshacemos de la ventana Login
            dispose();
        }
    
        // DE CASO CONTRARIO A LOS ANTERIORES
        else {
            // Creamos un ImageIcon pasándole el nombre del icono que se encuen-
            // tra dentro de la carpeta de este proyecto
            miImagen =new ImageIcon(getClass().getResource("/icons/block.png"));
            // Enseamos el mensaje de advertencia, pasándo como parámetros el 
            // mensaje que se mostrará, el título de la ventana de advertencia,
            // el tipo de panel(de advertencia) y la imagen que mostrará.
            JOptionPane.showMessageDialog(null, "Usuario y/o "
                    + "contraseña no válida.","Acceso no permitido",
                    JOptionPane.WARNING_MESSAGE, miImagen);
        }
    }
    
    
    /**
     * infoActionPerformed.
     * @param evt 
     */
    private void infoActionPerformed(ActionEvent evt){
            miImagen =new ImageIcon(getClass().getResource("/icons/info.png"));
            JOptionPane.showMessageDialog(null, "Bienvenido a Biomed V.1. Esta "
                    + "aplicación es de uso exclusivo para el centro de "
                    + "administración de la empresa BIO-TEC.\n"
                    + "Una aplicación desarrollada por la UPV. Todos los "
                            + "derechos reservados ©.","Acerca del programa",
                    JOptionPane.WARNING_MESSAGE, miImagen);
    }
    
    
    /**
     * entrarTecla. Análogo al método aceptarActionPerforme.
     * @param evt 
     */
    private void entrarTecla(java.awt.event.KeyEvent evt){
        
        // Si el código de tecla obtenido es ENTER...
        if(evt.getKeyCode()==KeyEvent.VK_ENTER){
           
        String Usuario="maescl@gerente.com";
        String Contraseña="13579";
        
        String Usuario2="lealecvi@admin.com";
        String Contraseña2="08642";
        
        String Pass=new String(CampoC.getPassword());
        String Pass2=new String(CampoC.getPassword());

        if(CampoU.getText().equals(Usuario) && Pass.equals(Contraseña)) {
            ScreenSplash SSA = new ScreenSplash();
            SSA.activarGerente();
            dispose(); 
        }
        
        else if(CampoU.getText().equals(Usuario2) && Pass2.equals(Contraseña2)){
            ScreenSplash SSA = new ScreenSplash();
            SSA.activarAdmin();
            dispose();
        }

        else {
                miImagen =new ImageIcon(getClass().getResource("/icons/block.png"));
                JOptionPane.showMessageDialog(null, "Usuario y/o "
                + "contraseña no válida.","Acceso no permitido",
                JOptionPane.WARNING_MESSAGE, miImagen);
            }
        }
    }
    

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Login biomed = new Login();   
    }
}
