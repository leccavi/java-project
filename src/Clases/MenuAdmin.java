/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;

import java.awt.*;
import java.util.Calendar;
import static java.util.Calendar.YEAR;
import javax.swing.*;
import java.awt.event.*;
import java.util.GregorianCalendar;

/**
 *
 * @author LeandroLV
 */
public class MenuAdmin extends JFrame{
    public JLabel tituloAdmin; 
    public JButton btnChat;
    public JButton cerrarSes;
   
    public Image pruebas;
    public ImageIcon miImagen;
    public JLabel titCer;
    public JLabel titChat;
    public JLabel HORA;
    public JLabel fecha;
    
    // Creamos un nuevo calendario, necesario para la fecha y hora
    private Calendar obj = new GregorianCalendar();
    
    // Referenciamos datos de tipo int, que utilizaremos en el método relojito
    private int segundo; 
    private int minuto;
    private int hora;
    
    /**
     * MenuAdmin.
     */
    public MenuAdmin(){
        relojito();
        setTitle("Menú principal");
        setSize(900,600);
        iniciar();
        setVisible(true);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        initComponents(); //iniciar los controles o componentes
    }
    
   
    /**
     * iniciar.
     */
    public void iniciar(){
        setLocationRelativeTo(null);
    }
    
    /**
     * initComponents.
     */
    public void initComponents(){
        
        getContentPane().setLayout(null);
        setResizable(false);
        
        
        // PROGRAMA TITULO //
        tituloAdmin = new JLabel();
        tituloAdmin.setText("Bienvenido Leandro"); 
        tituloAdmin.setFont(new Font("Orbitron", 5, 28));
        tituloAdmin.setBounds(293, 35, 370, 50);
        getContentPane().add(tituloAdmin);

        
        // BOTON DE CERRAR SESIÓN //
        miImagen = new ImageIcon(getClass().getResource("/icons/lock.png"));
        cerrarSes = new javax.swing.JButton(miImagen);
        cerrarSes.setBackground(Color.white);
        cerrarSes.setBorderPainted(false);
        cerrarSes.setToolTipText("Cerrar sesión");
        getContentPane().add(cerrarSes);
        cerrarSes.setFont(new java.awt.Font("Helvetica", 0, 16));
        cerrarSes.setBounds(150, 400, 110, 110);
        
        // evento del botón aceptar
        cerrarSes.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                cerrarSesActionPerformed(evt);
                
            }
        });
        
        titCer = new JLabel("Cerrar sesión");
        titCer.setHorizontalAlignment(SwingConstants.CENTER);
        titCer.setFont(new java.awt.Font("", 5, 16));
        titCer.setBounds(150, 485, 110, 110);
        getContentPane().add(titCer);
        
        
        
        // ***FECHA*** //
        // Hemos decidido poner fecha en el menú, pues consideramos de gran 
        // importancia recordar el día a nuestro cliente, ya que puede situarse
        // dentro del espacio temporal, que complementaremos más adelante con
        // la hora...
        
        // Vayamos manos a la obra. Primeramente tenemos que crear un vector de
        // tipo String referenciado por meses. Dentro del vector escribimos los
        // meses que tiene un año terrestre.
        String meses[] = {"Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", 
            "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", 
            "Diciembre"};
        
        // Y de forma análoga para los días.
        String dias[] = {"Lunes", "Martes", "Miércoles", "Jueves", 
            "Viernes", "Sábado", "Domingo"};
        
        int dia = obj.get(Calendar.DAY_OF_MONTH);
        
        // JAVA GARANTIZA QUE LOS VALORES DE DOMINGO A SÁBADO SON CONTIGUOS
        // DEL (1 AL 7). ESTO ES INTERESANTE SABERLO POR LO QUE VEREMOS UN POCO
        // MÁS ADELANTE
        int diaSemana = obj.get(Calendar.DAY_OF_WEEK);
        // OBTENEMOS EL MES DEL CALENDARIO
        int mes = obj.get(Calendar.MONTH);
        // OBTENEMOS EL AÑO DEL CALENDARIO
        int año = obj.get(Calendar.YEAR);
        
        // CREAMOS LA ETIQUETA...
        fecha = new JLabel();
        // Y AHORA LE PONEMOS LA FECHA... VEMOS QUE PARA EL DIA:
        // dias[diaSemana-1]
        // EN NUESTRO VECTOR DÍAS: LUNES(0), MARTES(1), MIÉRCOLES(2), JUEVES(3),
        // VIENRES(4), SÁBADO(5), DOMINGO(6). 
        // MÁS SIN EMBARGO, JAVA OBTIENE LOS DÍAS SEGÚN EL ÍNDICE:
        // LUNES(2), MARTES(3), MIÉRCOLES(4), JUEVES(5), VIERNES(6), SÁBADO(7) 
        // Y DOMINGO(1). INICIA DESDE EL DOMINGO.
        // POR ELLO... SI QUEREMOS OBTENER EL DÍA DE HOY, TENDREMOS QUE HACER:
        // diaSemana-2
        // POR EJEMPLO, HOY ESTAMOS A DÍA MIÉRCOLES, Y JAVA OBTIENE MIÉRCOLES 
        // CON ÍNDICE 4, PERO EN NUESTRO VECTOR DE DÍAS TIENE ÍNDICE 2... ASÍ
        // ENTONCES, SI AL 4 LE RESTAMOS 2, OBTENDREMOS 2, EL ÍNDICE QUE TIENE
        // MIÉRCOLES EN NUESTRO VECTOR...
        fecha.setText("Hoy es "+dias[diaSemana-2]+", "+dia+" de "
                +meses[mes]+" de "+año);
        fecha.setBounds(334, 80, 250, 20);
        getContentPane().add(fecha);
        
        // PREPARAMOS EL COMPONENTE HORA... 
        HORA = new JLabel();
        HORA.setBounds(416, 107, 100, 20);
        getContentPane().add(HORA);
        
        
        
        // BOTON DE CHAT //
        miImagen = new ImageIcon(getClass().getResource("/icons/comments.png"));
        btnChat = new JButton(miImagen);
        btnChat.setBackground(Color.white);
        btnChat.setBorderPainted(false);
        btnChat.setToolTipText("Acceda al chat");   
        getContentPane().add(btnChat);
        btnChat.setFont(new Font("Helvetica", 0, 16));
        btnChat.setBounds(640, 400, 110, 110);
        btnChat.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                btnChatActionPerformed(evt);     
            }
        });
        
        titChat = new JLabel("Chat");
        titChat.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        titChat.setFont(new java.awt.Font("", 5, 16));
        titChat.setBounds(640, 485, 110, 110);
        getContentPane().add(titChat);

        PanelImagen p= new PanelImagen("/Imagenes/Splash.jpg");
        getContentPane().add(p);
        p.setBounds(0, 0, 900, 600);      
    }
    
    
    /**
     * relojito. Método que nos permitirá iniciar el reloj.
     */
    private void relojito() { 

        // CREAMOS UN TIMER QUE NOS PERMITIRÁ QUE EL RELOJ VAYA CONTANDO 
        // LOS SEGUNDOS, MINUTOS Y HORAS EN TIEMPO REAL
        // CUANDO LO CREAMOS, LE PASAMOS COMO PARÁMETRO LA CANTIDAD DE MILISE-
        // GUNDOS QUE DEBERÁ ESPERAR (RETARDO) Y UN ESCUCHADOR DE EVENTOS
        Timer timer = new Timer(1000, new ActionListener() { 
            
        @Override 
        public void actionPerformed(ActionEvent ae) { 
            // OBTENEMOS LA FECHA
        java.util.Date actual = new java.util.Date(); 
        // LE PONEMOS AL CALENDARIO LA FECHA ACTUAL (LA DE HOY)
        obj.setTime(actual); 
        // OBTENEMOS LA HORA DEL DIA
        hora = obj.get(Calendar.HOUR_OF_DAY); 
        // OBTENEMOS LOS MINUTOS
        minuto = obj.get(Calendar.MINUTE); 
        // OBTENEMOS LOS SEGUNDOS
        segundo = obj.get(Calendar.SECOND); 
        // ESCRIBIMOS UN STRING DE FORMATO, ACOMODANDO A NUESTRO GUSTO LA HORA,
        // MINUTOS Y SEGUNDOS
        String hour = String.format("%02d : %02d : %02d", hora, minuto, 
                segundo); 
        // Y EL TOQUE FINAL... AÑADIMOS A NUESTRA ETIQUETA EL STRING HOUR
        HORA.setText(hour); 
        } 
    }); 
        // PARA VER VISUALMENTE COMO PASAN LOS SEGUNDOS, ACTIVAMOS EL TIMER...
        timer.start(); 
    }
    
    
    /**
     * btnChatActionPerformed.
     * @param evt 
     */
    public void btnChatActionPerformed(ActionEvent evt){
        JChat Ct = new JChat();
        Ct.iniciar();
        Ct.conectar();
        Ct.arrancaReceptor();
        Ct.setVisible(true); 
    }
    
    /**
     * cerrarSesActionPerformed.
     * @param evt 
     */
    public void cerrarSesActionPerformed(ActionEvent evt){
        Login log = new Login();
        dispose();
    }
    
    public static void main(String []args){
        new MenuAdmin();
    }

}