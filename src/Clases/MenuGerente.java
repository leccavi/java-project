/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;

import java.awt.*;
import java.util.Calendar;
import java.util.GregorianCalendar;
import javax.swing.*;
import java.awt.event.*;

/**
 *
 * @author LeandroLV
 */
public class MenuGerente extends JFrame{
    
    // Referenciamos los componentes que utilizaremos
    private JLabel tituloGerente; 
    private JButton addTb;
    private JButton btnChat;
    private JButton cerrarSes;
    private ImageIcon miImagen;
    private JLabel titCer;
    private JLabel titChat;
    private JLabel fecha;
    private JLabel HORA;
    private JLabel titADDTB;
    
    // Creamos un nuevo calendario, necesario para la fecha y hora
    private Calendar obj = new GregorianCalendar();
    
    // Referenciamos datos de tipo int, que utilizaremos en el método relojito
    private int segundo; 
    private int minuto;
    private int hora;
    
       
    public MenuGerente(){
        
        // Invocamos al método relojito para que nos inicie el reloj
        relojito();
        
        // Añadimos titulo a nuestra ventana
        setTitle("Menú principal");
        
        // Definimos el tamaño que deseamos que tenga la ventana
        setSize(900,600);
        
        // Invocamos al método inciar, método que nos centrará la ventana en
        // el centro de la pantalla
        iniciar();
        
        // Hacemos visible la ventana
        setVisible(true);
        
        // Si cierra, finaliza la ejecución del programa
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        // Invocamos a este método para que situe todos nuestros componentes
        // en nuestra ventana
        initComponents();
        
    }
    
    /**
     * Método para centrar ventana. 
     */
    public void iniciar(){
        // Situamos la ventana en el centro de la pantalla con "null"
        setLocationRelativeTo(null);
    }
    
    /**
     * initComponents. Nos situará los componentes en nuestra ventana.
     */
    public void initComponents(){
        
        // Ponemos Layout---> NULL... Esto nos permetirá tener la capacidad de
        // poner nuestros componentes en el sitio que queramos...
        getContentPane().setLayout(null);
        
        // La ventana no se podrá estirar, ni maximizar
        setResizable(false);
        
        
        // TITULO DE BIENVENIDA //
        tituloGerente = new JLabel();
        // DEFINIMOS UN TEXTO PARA LA ETIQUETA //
        tituloGerente.setText("Bienvenida María");
        // PROGRAMAMOS LA FUENTE QUE QUEREMOS QUE TENGA, SU ESTILO Y SU TAMAÑO.
        // La fuente es una que hemos descargado de internet y añadido en 
        // nuestra biblioteca de fuentes...
        tituloGerente.setFont(new Font("Orbitron", 5, 28));
        // Como consecuencia del setLayout(null), podemos utilizar el método
        // set bounds... (eje X, eje Y, ancho del componente, vertical del 
        // componente).
        tituloGerente.setBounds(328, 35, 370, 50);
        // Añadimos nuestro componente a este panel--> nuestra ventana
        getContentPane().add(tituloGerente);
        
        
        // ** DE FORMA ANÁLOGA PARA EL RESTO DE COMPONENTES ** //
        
        
        // BOTON DE AÑADIR TRABAJADOR //
        // creamos un nuevo ImageIcon (imagen de icono), pasándole el nombre
        // de nuestro icono; icono que se encontrará dentro de la carpeta de
        // este proyecto
        miImagen = new ImageIcon(getClass().getResource("/icons/users.png"));
        // creamos el botón addTb, asociándolo con el icono que hemos
        // creado y cargado 
        addTb = new JButton(miImagen);
        // Por cuestión de estética, ponemos al boton un fondo de color blanco.
        // En WINDOWS se ve el fondo, sin embargo, en la versión MAC, no 
        // aparece, lo que hace, aún más, bonita la app.
        addTb.setBackground(Color.white);
        // Si nos paramos a observar, los botones llevan un contorno que los 
        // rodea para caracterizarlos y separarlos/realzarlos del contenedor en
        // donde se encuentran. Por cuestión de estética, nosotros hemos 
        // decidido eliminar ese contorno/borde del botón --> (false)
        addTb.setBorderPainted(false);
        addTb.setToolTipText("Acceda a añadir trabajador");
        getContentPane().add(addTb);
        addTb.setBounds(395, 175, 110, 110);
        
        // AÑADIMOS AL BOTÓN UN ESCUCHADOR DE EVENTO (QUE SE ENCARGARÁ
        // DE ANALIZAR QUÉ EVENTO SE HA PRODUCIDO) Y LO CREAMOS...
        // CONCRETAMENTE, ACTIONLISTENER PERMITE A UN COMPONENTE
        // (ADDTB) RESPONDER A LAS ACCIONES QUE OCURREN EN ÉL (EVENTOS DE TIPO
        // ACTION EVENT), COMO UN CLICK SOBRE UN BOTÓN
        addTb.addActionListener(new ActionListener() {      
            @Override
            public void actionPerformed(ActionEvent evt) {
                // ASOCIAMOS EL MÉTODO QUE SE ENCARGARÁ DE TRATAR EL EVENTO, O
                // QUE ES LO MISMO: QUE HAGA LO QUE QUERAMOS AL PRODUCIRSE EL
                // EVENTO CAPTURADO POR EL ESCUCHADOR.
                addTbActionPerformed(evt);   
            }
        });
        
        titADDTB = new JLabel("Trabajadores");
        titADDTB.setHorizontalAlignment(SwingConstants.CENTER);
        titADDTB.setFont(new Font("Helvetica", 5, 16));
        titADDTB.setBounds(395, 260, 110, 110);
        getContentPane().add(titADDTB);
        
        
        
        // BOTON DE CERRAR SESIÓN //
        miImagen = new ImageIcon(getClass().getResource("/icons/lock.png"));       
        cerrarSes = new JButton(miImagen);    
        cerrarSes.setBackground(Color.white);            
        cerrarSes.setBorderPainted(false);    
        // Ayuda que aparecerá al poner el ratón encima del botón durante
        // un corto tiempo
        cerrarSes.setToolTipText("Cerrar sesión");
        getContentPane().add(cerrarSes);
        cerrarSes.setBounds(150, 400, 110, 110);
        
        // ANÁLOGO AL BOTÓN ADDTB
        cerrarSes.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                // Método que tendrá lugar cuando se produzca el evento de tipo
                // ActionEvent (como un click, por ejemplo)
                cerrarSesActionPerformed(evt);        
            }
        });
        
        
        // TITULO CERRAR SESIÓN //
        titCer = new JLabel("Cerrar sesión");
        // CENTRAMOS LA ETIQUETA DENTRO DE SU TAMAÑO DE ANCHO
        titCer.setHorizontalAlignment(SwingConstants.CENTER);
        titCer.setFont(new Font("Helvetica", 5, 16));
        titCer.setBounds(150, 485, 110, 110);
        getContentPane().add(titCer);
        
        
        // ***FECHA*** //
        // Hemos decidido poner fecha en el menú, pues consideramos de gran 
        // importancia recordar el día a nuestro cliente, ya que puede situarse
        // dentro del espacio temporal, que complementaremos más adelante con
        // la hora...
        
        // Vayamos manos a la obra. Primeramente tenemos que crear un vector de
        // tipo String referenciado por meses. Dentro del vector escribimos los
        // meses que tiene un año terrestre.
        String meses[] = {"Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", 
            "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", 
            "Diciembre"};
        
        // Y de forma análoga para los días.
        String dias[] = {"Lunes", "Martes", "Miércoles", "Jueves", 
            "Viernes", "Sábado", "Domingo"};
        
        int dia = obj.get(Calendar.DAY_OF_MONTH);
        
        // JAVA GARANTIZA QUE LOS VALORES DE DOMINGO A SÁBADO SON CONTIGUOS
        // DEL (1 AL 7). ESTO ES INTERESANTE SABERLO POR LO QUE VEREMOS UN POCO
        // MÁS ADELANTE
        int diaSemana = obj.get(Calendar.DAY_OF_WEEK);
        // OBTENEMOS EL MES DEL CALENDARIO
        int mes = obj.get(Calendar.MONTH);
        // OBTENEMOS EL AÑO DEL CALENDARIO
        int año = obj.get(Calendar.YEAR);
        
        // CREAMOS LA ETIQUETA...
        fecha = new JLabel();
        // Y AHORA LE PONEMOS LA FECHA... VEMOS QUE PARA EL DIA:
        // dias[diaSemana-1]
        // EN NUESTRO VECTOR DÍAS: LUNES(0), MARTES(1), MIÉRCOLES(2), JUEVES(3),
        // VIENRES(4), SÁBADO(5), DOMINGO(6). 
        // MÁS SIN EMBARGO, JAVA OBTIENE LOS DÍAS SEGÚN EL ÍNDICE:
        // LUNES(2), MARTES(3), MIÉRCOLES(4), JUEVES(5), VIERNES(6), SÁBADO(7) 
        // Y DOMINGO(1). INICIA DESDE EL DOMINGO.
        // POR ELLO... SI QUEREMOS OBTENER EL DÍA DE HOY, TENDREMOS QUE HACER:
        // diaSemana-2
        // POR EJEMPLO, HOY ESTAMOS A DÍA MIÉRCOLES, Y JAVA OBTIENE MIÉRCOLES 
        // CON ÍNDICE 4, PERO EN NUESTRO VECTOR DE DÍAS TIENE ÍNDICE 2... ASÍ
        // ENTONCES, SI AL 4 LE RESTAMOS 2, OBTENDREMOS 2, EL ÍNDICE QUE TIENE
        // MIÉRCOLES EN NUESTRO VECTOR...
        fecha.setText("Hoy es "+dias[diaSemana-2]+", "+dia+" de "
                +meses[mes]+" de "+año);
        fecha.setBounds(330, 80, 400, 20);
        getContentPane().add(fecha);
        
        // PREPARAMOS EL COMPONENTE HORA... 
        HORA = new JLabel();
        HORA.setBounds(416, 107, 100, 20);
        getContentPane().add(HORA);
        
        
        
        // BOTON DE CHAT //
        miImagen = new ImageIcon(getClass().getResource("/icons/comments.png"));
        btnChat = new JButton(miImagen);
        btnChat.setBackground(Color.white);
        btnChat.setBorderPainted(false);
        btnChat.setToolTipText("Acceda al chat");   
        getContentPane().add(btnChat);
        btnChat.setFont(new Font("Helvetica", 0, 16));
        btnChat.setBounds(640, 400, 110, 110);
        
        // evento del botón chat
        btnChat.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                btnChatActionPerformed(evt);     
            }
        });
        
        
        // TÍTULO DE CHAT //
        titChat = new JLabel("Chat");
        titChat.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        titChat.setFont(new java.awt.Font("", 5, 16));
        titChat.setBounds(640, 485, 110, 110);
        getContentPane().add(titChat);

        PanelImagen p= new PanelImagen("/Imagenes/Splash.jpg");
        getContentPane().add(p);
        p.setBounds(0, 0, 900, 600);

    }
    
    /**
     * cerrarSesActionPerformed. Mediante este método, que tratará el evento
     * de tipo ActionEvent producido, podremos cerrar la sesión, volviendo al 
     * login o ingreso principal.
     * @param evt 
     */
    public void cerrarSesActionPerformed(ActionEvent evt){
        // Creamos un nuevo objeto de Login. El constructor de Login posee
        // ya todos los métodos necesarios para visualizarse como hemos deseado
        Login log = new Login();
        // con dispose(); cerramos la ventana de menú, de esta manera solo
        // tendremos una ventana: la del login
        dispose();
    }
    
    /**
     * btnChatActionPerformed. Mediante este método abriremos la ventana de CHAT
     * @param evt 
     */
    public void btnChatActionPerformed(ActionEvent evt){
        // Creamos un nuyevo objeto de la clase JChat
        JChat Ct = new JChat();
        // Invocamos a los métodos conectar y arrancaReceptor de JChat
        Ct.conectar();
        Ct.arrancaReceptor();
        Ct.setVisible(true);  
        // no hemos puestos dispose puesto que queremos conservar la ventana de
        // menú principal
    }
    
    /**
     * addTbActionPerformed. Mediante este método abriremos la ventana de 
     * gestión de trabajadores
     * @param evt 
     */
    public void addTbActionPerformed(ActionEvent evt){
        // Creamos un nuevo objeto de la clase
        addTb gestionTrabajadores = new addTb();
    }
    
    /**
     * relojito. Método que nos permitirá iniciar el reloj.
     */
    private void relojito() { 
        
    
        // CREAMOS UN TIMER QUE NOS PERMITIRÁ QUE EL RELOJ VAYA CONTANDO 
        // LOS SEGUNDOS, MINUTOS Y HORAS EN TIEMPO REAL
        // CUANDO LO CREAMOS, LE PASAMOS COMO PARÁMETRO LA CANTIDAD DE MILISE-
        // GUNDOS QUE DEBERÁ ESPERAR (RETARDO) Y UN ESCUCHADOR DE EVENTOS
        Timer timer = new Timer(1000, new ActionListener() { 
            
        @Override 
        public void actionPerformed(ActionEvent ae) { 
            // OBTENEMOS LA FECHA
        java.util.Date actual = new java.util.Date(); 
        // LE PONEMOS AL CALENDARIO LA FECHA ACTUAL (LA DE HOY)
        obj.setTime(actual); 
        // OBTENEMOS LA HORA DEL DIA
        hora = obj.get(Calendar.HOUR_OF_DAY); 
        // OBTENEMOS LOS MINUTOS
        minuto = obj.get(Calendar.MINUTE); 
        // OBTENEMOS LOS SEGUNDOS
        segundo = obj.get(Calendar.SECOND); 
        // ESCRIBIMOS UN STRING DE FORMATO, ACOMODANDO A NUESTRO GUSTO LA HORA,
        // MINUTOS Y SEGUNDOS
        String hour = String.format("%02d : %02d : %02d", hora, minuto, 
                segundo); 
        // Y EL TOQUE FINAL... AÑADIMOS A NUESTRA ETIQUETA EL STRING HOUR
        HORA.setText(hour); 
        } 
    }); 
        // PARA VER VISUALMENTE COMO PASAN LOS SEGUNDOS, ACTIVAMOS EL TIMER...
        timer.start(); 
    }
    
    
    public static void main (String []args){
        new MenuGerente();
    }
}