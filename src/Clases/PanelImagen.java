/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;

import java.awt.*;
import javax.swing.*;

/**
 *
 * @author LeandroLV
 */
public class PanelImagen extends JPanel{
    // Definimos atributos
    ImageIcon imagen;
    String nombre;
    
    /**
     * Método constructor de PanelImagen.
     * @param nombre 
     */
    public PanelImagen(String nombre){
        this.nombre=nombre;
    }
    
    /**
     * Método paint. Sobreescribimos el método paint (método de la clase padre)
     * @param g 
     */
    @Override
    public void paint(Graphics g){
        Dimension tamaño=getSize();
        
        // Creamos el objeto de ImageIcon (referenciado por Imagen).
        // Obtenemos el recurso, es decir, la imagen, pasando un String 
        // indicando dónde se encuentra ésta. Además, obtenemos el tipo de imagen 
        // (Jpeg, png...). 
        // La ruta de donde se encuentra la introduciremos al  crear algún 
        // objeto de esta clase gracias al constructor.
        imagen=new ImageIcon(getClass().getResource(nombre));
        
        // llamamos al método drawImage de Graphics para que dibuje la imagen. 
        // drawImage( IMAGEN QUE HEMOS CREADO Y CARGADO DESDE LA RUTA QUE HEMOS
        // ESPECIFICADO, POSICIÓN EJE X DE LA PARTE SUPERIOR IZQUIERDA DE LA 
        // IMAGEN, POSICIÓN EJE Y DE LA PARTE SUPERIOR IZQUIERA DE LA IMAGEN, 
        // NULL [El parámetro observador notifica a la aplicación de cambios a 
        // una imagen que se carga de forma asíncrona . El parámetro observador 
        // no se utiliza con frecuencia]. 
        g.drawImage(imagen.getImage(), 0, 0, tamaño.width, tamaño.height, null);
        
        // Decimos que no sea opaco
        setOpaque(false);
        
        // pinta el dibujo, obtenido de drawImage, mediante las pinturas que 
        // necesita el dibujo para ser visible
        super.paint(g);
    }
    
}
