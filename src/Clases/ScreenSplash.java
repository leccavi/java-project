/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
/**
 *
 * @author LeandroLV
 */
public class ScreenSplash extends JFrame{
    //Atributos
    
    private Timer tiempo; // timer necesario para activar la barra de progreso
    int cont; // referenciamos un int que nos servirá como contador
    public final static int TIEMPO=15; // ponemos un valor estático para el
    // tiempo que tardará en cargar nuestro progress bar
    public JProgressBar barrita; // barra de carga
    public JLabel titulo; // titulo

    /**
     * Método constructor ScreenSplash.
     */
    public ScreenSplash(){
        // Quitamos el marco (minimizar, maximizar y cerrar) de la ventana
        setUndecorated(true);
        // Definimos tamaño de la ventana
        setSize(900,600); 
        // Método para centrar la ventana en la pantalla
        iniciar();       
        // Iniciamos nuestros componentes
        initComponents();  
        // La hacemos visible
        setVisible(true);       
    }
    
    /**
     * Método inciar.
     */
    final void iniciar(){
        setLocationRelativeTo(null);
    }
    
    /**
     * initComponents.
     */
    final void initComponents(){
        // No ponemos Layout... de esta manera podremos poner nuestros 
        // componenes a nuestro gusto
        getContentPane().setLayout(null);
        // No se podrá estirar
        setResizable(false);
        
        // BARRA DE PROGRESO //
        barrita = new JProgressBar();
        getContentPane().add(barrita);
        barrita.setBounds(350, 300, 200, 30);
       
        
        // TITULO DE CARGA //
        titulo = new JLabel();
        titulo.setText("Cargando su información");
        titulo.setHorizontalAlignment(SwingConstants.CENTER);
        titulo.setFont(new Font("Verdana", 5, 27));
        titulo.setBounds(250, 200, 400,40);
        getContentPane().add(titulo);

    
        // BACKGROUND //
        PanelImagen p= new PanelImagen("/Imagenes/Splash.jpg");
        getContentPane().add(p);
        p.setBounds(0, 0, 900, 600); 
        
    }
    
    
    // CREAMOS LA CLASE TIMERLISTENERADMIN, E IMPLEMENTAMOS UN ESCUCHADOR DE 
    // EVENTOS DE TIPO ACTIONEVENT
    class TimerListenerAdmin implements ActionListener{

    @Override
    // Recibirá eventos de tipo ActioneEvent. Este método será para cuando
    // ingrese el usuario Administrador
    public void actionPerformed(ActionEvent e) {
        // Vamos incrementando el int cont
        cont++;
        // A la barrita le vamos pasando los valores del cont
        barrita.setValue(cont);
        // Si el cont llega a 100...
        if(cont==100){
            // La barrita parará con el método .stop(); del Timer
            tiempo.stop();
            // Cuando pare abriremos el menú Administrador
            MenuAdmin MNA = new MenuAdmin();
            // Cerraremos la ventana de ScreenSplash
            dispose();
            }
        }
    }
    
  
   
    /**
     * activarAdmin. Este método será invocado desde la clase login, dentro
     * del tratamiento de los eventos que se produzcan tanto de teclado como de
     * click.
     */
    public void activarAdmin(){
        // Iniciamos el cont desde 0, por ello ponemos el cont en -1 (1-1=0)
        cont = -1;
        // Añadimos a la barrita el valor de 0
        barrita.setValue(0);
        // Por estética, programamos que en la barrita vaya apareciendo el
        // porcentaje de carga
        barrita.setStringPainted(true);
        // Y AQUÍ ESTÁ LA CLAVE... CREAMOS EL TIMER Y LE PASAMOS EL TIEMPO QUE
        // TARDARÁ EN CARGAR Y UN OBJETO DE LA CLASE QUE IMPLEMENTA EL 
        // ESCUCHADOR...
        tiempo = new Timer(TIEMPO, new TimerListenerAdmin());
        // Y AHORA INICIAMOS EL TIEMPO, Y ESTO PRODUCIRÁ EL TRATAMIENTO DEL 
        // EVENTO, YA QUE .START ES UN EVENTO DE TIPO ACTIONEVENT
        tiempo.start();
    
    }
    
    // DE FORMA ANÁLOGA PARA EL GESTOR //
    
    class TimerListenerGestor implements ActionListener{

    @Override
    public void actionPerformed(ActionEvent e) {
        cont++;
        barrita.setValue(cont);
        if(cont==100){
            tiempo.stop();
            MenuGerente MGE = new MenuGerente();
            dispose();
            }
        }
    }
    
    /**
     * activarGerente.
     */
    public void activarGerente(){
        cont = -1;
        barrita.setValue(0);
        barrita.setStringPainted(true);
        tiempo = new Timer(TIEMPO, new TimerListenerGestor());
        tiempo.start();
    } 
}
    
  