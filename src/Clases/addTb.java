/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;

import com.sun.glass.events.KeyEvent;
import java.awt.*;
import java.awt.event.*;
import java.sql.*;
import javax.swing.*;
import static javax.swing.JOptionPane.showConfirmDialog;
import javax.swing.event.*;
import javax.swing.table.*;

/**
 *
 * @author LeandroLV
 */
public class addTb extends JFrame{
    
    
    // Definimos atributos 
    
    private JLabel Titulo;
    private JLabel nombre;
    private JLabel apellidos;
    private JLabel Dni;
    private JLabel edad;
    private JLabel telefono;
    private JLabel profesion;
    private JLabel email;
    private JLabel usuario;
    private JLabel contraseña;
    private JLabel discapacidad;
    private JTextField campoN;
    private JTextField campoA;
    private JTextField campoD;
    private JTextField campoEdad;
    private JTextField campoTel;
    private JComboBox prof;
    private JTextField campoEmail;
    private JTextField campoContraseña;
    private JCheckBox campoDiscapacidad;
    private JButton nuevo;
    private JButton guardar;
    private JButton cancelar;
    private JButton vaciar;
    private JButton eliminar;
    private JButton modificar;
    private JButton Imprimir;
    private JTable jtblDatos;
    private JScrollPane scroll;
    private JButton limpiar;
    private ImageIcon miImagen;
    private JButton refresh;
    
    // Referenciamos el modelo de la tabla que crearemos...
    DefaultTableModel xd;
    
    /**
     * Método constructor addTb.
     */
    public addTb(){
        
        // Añadimos titulo
        setTitle("Administración");
        
        // Designamos tamaño
        setSize(900,600);
        
        // Llamamos al método iniciar
        iniciar();
        
        // Iniciamos los componentes
        initComponents(); 
        
        // Método deshabilitar 
        deshabilitar();
        
        // Hacer visible la ventana
        setVisible(true);
        
        // Ver en la tabla todos los datos de la tabla trabajadores de la base
        // de datos biomed
        visualizar();
    }
    
    /**
     * Método iniciar. Ponemos la ventana en el centro de la pantalla
     */
    public void iniciar(){
        setLocationRelativeTo(null);
    }
    
    /**
     * Método iniciar componentes. Colocamos los componentes en la ventana.
     */
    public void initComponents(){
        
        // Layout(null): nos permite colocar los componentes donde queramos
        getContentPane().setLayout(null);
        
        // Quitamos la capacidad de alargar/hacer grande la ventana
        setResizable(false);
        
        // Creamos el componente
        Titulo = new javax.swing.JLabel();
        // Designamos titulo al componente
        Titulo.setText("Gestión de trabajadores");
        // Ponemos fuente al componente
        Titulo.setFont(new Font("Arial", 5, 20));
        // Y lo más importante... podemos poner el componente donde queramos 
        // (eje X, eje Y, tamaño horizontal del componente, tamaño vertical
        // del componente)
        Titulo.setBounds(20, 10, 250, 50);
        // Añadimos el componente a el panel de la ventana
        getContentPane().add(Titulo);
        
        
        // !!*** DE FORMA ANÁLOGA PARA TODOS LOS COMPONENTES ***!! \\
        
        
        nombre = new JLabel();
        nombre.setText("Nombre:");
        nombre.setFont(new Font("Arial", 5, 16));
        nombre.setBounds(20, 80, 70, 18);
        getContentPane().add(nombre);
        
        
        campoN = new JTextField();
        campoN.setFont(new Font("Arial", 5, 14));
        campoN.setBounds(120, 80, 150, 20);
        getContentPane().add(campoN);
        
        // SUN DEFINE EL ENFOQUE TRANSVERSAL COMO "LA CAPACIDAD DEL USUARIO 
        // PARA CAMBIAR EL ENFOQUE SIN MOVER EL CURSOR".
        // INDICAMOS AL campoN QUÉ TECLAS QUEREMOS QUE SEAN LAS QUE SIRVAN
        // PARA TRANSFERIR EL FOCO. PARA ELLO LLAMAMOS AL MÉTODO 
        // setFocusTraversalKeys(), PASÁNDOLE EL ENFOQUE TRANSVERSAL MEDIANTE
        // TECLADO (TAB). EN DFINITIVA, ESTO PERMITE QUE CON EL TABULADOR 
        // PODAMOS PASAR AL FOCO QUE SE NOS DE LA GANA, EN LUGAR DE QUE PASE EL 
        // FOCO AL COMPONENTE MÁS CERCANO...
        campoN.setFocusTraversalKeys(KeyboardFocusManager.
                FORWARD_TRAVERSAL_KEYS, java.util.Collections.EMPTY_SET);
        
        // PARA QUE PUEDA SER POSIBLE EL ENFOQUE TRANSVERSAL MEDIANTE TECLADO,
        // DEBEMOS AÑADIR UN ESCUCHADOR A campoN DE TIPO TECLADO, ES DECIR, UN
        // ESCUCHADOR QUE ESTÉ A LA ESPERA DE RECIBIR EL EVENTO DE UNA TECLA
        // PRESIONADA, Y DE ESTA MANERA QUE PUEDA TRATAR DICHO EVENTO MEDIANTE
        // EL CORRESPONDIENTE MÉTODO QUE HEMOS DEFINIDO (MÉTODO QUE HARÁ LO QUE
        // NOSOTROS QUERAMOS, EN ESTE CASO, QUE PASE EL FOCO DE campoN A 
        // campoA).
        campoN.addKeyListener(new java.awt.event.KeyAdapter(){
            @Override
            public void keyPressed(java.awt.event.KeyEvent evt){
                // MÉTODO QUE TRATARÁ LA RESPUESTA AL EVENTO DE PRESIÓN DE TECLA
                campoNTeclaActionPerformed(evt);            
            }
        });
        
        
        apellidos = new JLabel();
        apellidos.setText("Apellidos:");
        apellidos.setFont(new Font("Arial", 5, 16));
        apellidos.setBounds(20, 120, 80, 18);
        getContentPane().add(apellidos);
        
        
        campoA = new JTextField();
        campoA.setFont(new Font("Arial", 5, 14));
        campoA.setBounds(120, 120, 150, 20);
        getContentPane().add(campoA);
        // ANÁLOGAMENTE AL campoN
        campoA.setFocusTraversalKeys(KeyboardFocusManager.
                FORWARD_TRAVERSAL_KEYS,  java.util.Collections.EMPTY_SET);
        campoA.addKeyListener(new java.awt.event.KeyAdapter(){
            @Override
            public void keyPressed(java.awt.event.KeyEvent evt){
                campoATeclaActionPerformed(evt);            
            }
        });
        
        
        Dni = new JLabel();
        Dni.setFont(new Font("Arial", 5, 16));
        Dni.setText("DNI:");
        Dni.setBounds(20, 160, 60, 18);
        getContentPane().add(Dni);
        
        
        campoD = new JTextField();
        campoD.setFont(new Font("Arial", 5, 14));
        campoD.setBounds(120, 160, 150, 20);
        getContentPane().add(campoD);
        // ANÁLOGAMENTE AL campoN
        campoD.setFocusTraversalKeys(KeyboardFocusManager.
                FORWARD_TRAVERSAL_KEYS, java.util.Collections.EMPTY_SET);
        campoD.addKeyListener(new java.awt.event.KeyAdapter(){
            @Override
            public void keyPressed(java.awt.event.KeyEvent evt){
                campoDTeclaActionPerformed(evt);          
            }
        });
        
        
        edad = new JLabel();
        edad.setFont(new Font("Arial", 5, 16));
        edad.setText("Edad:");
        edad.setBounds(20, 200, 70, 18);
        getContentPane().add(edad);
        
        
        campoEdad = new JTextField();
        campoEdad.setFont(new Font("Arial", 5, 14));
        campoEdad.setBounds(120, 200, 150, 20);
        getContentPane().add(campoEdad);
        // ANÁLOGAMENTE al campoN
        campoEdad.setFocusTraversalKeys(KeyboardFocusManager.
                FORWARD_TRAVERSAL_KEYS, java.util.Collections.EMPTY_SET);
        campoEdad.addKeyListener(new java.awt.event.KeyAdapter(){
            @Override
            public void keyPressed(java.awt.event.KeyEvent evt){
                campoEdadTeclaActionPerformed(evt);           
            }
        });
        
        
        telefono = new JLabel();
        telefono.setFont(new Font("Arial", 5, 16));
        telefono.setText("Teléfono:");
        telefono.setBounds(310, 80, 70, 18);
        getContentPane().add(telefono);
        
        
        campoTel = new JTextField();
        campoTel.setFont(new Font("Arial", 5, 14));
        campoTel.setBounds(410, 80, 150, 20);
        getContentPane().add(campoTel);
        // ANÁLOGAMENTE AL campoN
        campoTel.setFocusTraversalKeys(KeyboardFocusManager.
                FORWARD_TRAVERSAL_KEYS, java.util.Collections.EMPTY_SET);
        campoTel.addKeyListener(new java.awt.event.KeyAdapter(){
            @Override
            public void keyPressed(java.awt.event.KeyEvent evt){
                campoTelTeclaActionPerformed(evt);           
            }
        });
        
        
        profesion = new JLabel();
        profesion.setFont(new Font("Arial", 5, 16));
        profesion.setText("Profesión:");
        profesion.setBounds(310, 120, 100, 18);
        getContentPane().add(profesion);
        
        // Creamos un vector de tipo String
        String elemento[] = {"Médico", "Investigador", "Administrador", 
            "Gerente"};
        prof = new JComboBox(elemento);
        // No se podrá editar los ítems
        prof.setEditable(false);
        // Designamos el número máximo de elementos, que será 4
        prof.setMaximumRowCount(4);
        prof.setBounds(410, 120, 150, 22);
        getContentPane().add(prof);
        // ANÁLOGAMENTE AL campoN
        prof.setFocusTraversalKeys(KeyboardFocusManager.FORWARD_TRAVERSAL_KEYS,
                java.util.Collections.EMPTY_SET);
        prof.addKeyListener(new KeyAdapter(){
            @Override
            public void keyPressed(java.awt.event.KeyEvent evt){
                profTeclaActionPerformed(evt);            
            }
        });
        
        
        // Añadimos un escuchador que captará eventos de tipo ItemEvent... 
        // al seleccionar uno, produciremos un evento que será tratado por 
        // el método que crearemos
        prof.addItemListener(new ItemListener(){
            
            @Override
            public void itemStateChanged(ItemEvent evt) {
                
                // Método que tratará el evento
                profItemActionPerformed(evt);
            }
        });
        
        
        email = new JLabel();
        email.setFont(new Font("Arial", 5, 16));
        email.setText("Email:");
        email.setBounds(310, 160, 70, 18);
        getContentPane().add(email);
        
        
        campoEmail = new JTextField();
        campoEmail.setFont(new Font("Arial", 5, 14));
        campoEmail.setBounds(410, 160, 150, 20);
        getContentPane().add(campoEmail);
        
        
        
        miImagen = new ImageIcon(getClass().getResource("/icons/refresh24.png"));
        refresh = new JButton(miImagen);
        refresh.setBounds(560, 158, 24, 24);
        getContentPane().add(refresh);
        // Ayuda que aparecerá cuando el ratón permanezca encima del botón por
        // un corto tiempo
        refresh.setToolTipText("Actualice el EMAIL");
        // AÑADIMOS AL BOTÓN UN ESCUCHADOR DE EVENTO (QUE SE ENCARGARÁ
        // DE ANALIZAR QUÉ EVENTO SE HA PRODUCIDO) Y LO CREAMOS...
        // CONCRETAMENTE, ACTIONLISTENER PERMITE A UN COMPONENTE
        // (ADDTB) RESPONDER A LAS ACCIONES QUE OCURREN EN ÉL (EVENTOS DE TIPO
        // ACTION EVENT), COMO UN CLICK SOBRE UN BOTÓN
        refresh.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent evt) {
                // ASOCIAMOS EL MÉTODO QUE SE ENCARGARÁ DE TRATAR EL EVENTO, O
                // QUE ES LO MISMO: QUE HAGA LO QUE QUERAMOS AL PRODUCIRSE EL
                // EVENTO CAPTURADO POR EL ESCUCHADOR.
                refreshActionPerformed(evt);               
            }
        });
        
        
        contraseña = new JLabel();
        contraseña.setFont(new Font("Arial", 5, 16));
        contraseña.setText("Contraseña:");
        contraseña.setBounds(310, 200, 100, 18);
        getContentPane().add(contraseña);
        
        
        campoContraseña = new JTextField();
        campoContraseña.setFont(new Font("Arial", 5, 14));
        campoContraseña.setBounds(410, 200, 150, 20);
        getContentPane().add(campoContraseña);
        campoContraseña.setFocusTraversalKeys(KeyboardFocusManager.
                FORWARD_TRAVERSAL_KEYS,  java.util.Collections.EMPTY_SET);
        campoContraseña.addKeyListener(new KeyAdapter(){
            
            @Override
            public void keyPressed(java.awt.event.KeyEvent evt){
                campoContraseñaTeclaActionPerformed(evt);            
            }
        });
        
        
        nuevo = new JButton();
        nuevo.setFont(new Font("Arial", 5, 14));
        nuevo.setText("Nuevo");
        nuevo.setBounds(16, 250, 100, 30);
        getContentPane().add(nuevo);
        nuevo.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent evt) {
                nuevoActionPerformed(evt);         
            }
        });
      
        
        guardar = new JButton();
        guardar.setFont(new java.awt.Font("Arial", 5, 14));
        guardar.setText("Guardar");
        guardar.setBounds(116, 250, 100, 30);
        getContentPane().add(guardar);
        guardar.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent evt) {
                guardarActionPerformed(evt);  
            }
        });
        
        
        guardar.setFocusTraversalKeys(KeyboardFocusManager.
                FORWARD_TRAVERSAL_KEYS, java.util.Collections.EMPTY_SET);
        
        // AÑADIMOS UN ESCUCHADOR DE TECLADO, A LA ESPERA DE LA PERCEPCIÓN
        // DE UN EVENTO DE TECLADO (EN ESTE CASO DE PRESIONAR LA TECLA:
        // keyPressed).
        guardar.addKeyListener(new KeyAdapter(){
            
            @Override
            public void keyPressed(java.awt.event.KeyEvent evt){
                // Método que responderá al evento producido
                guardarTeclaActionPerformed(evt);       
            }
        });
        
        
       cancelar = new JButton();
       cancelar.setFont(new java.awt.Font("Arial", 5, 14));
       cancelar.setText("Cancelar");
       cancelar.setBounds(216, 250, 100, 30);
       getContentPane().add(cancelar);
       cancelar.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent evt) {
                cancelarActionPerformed(evt);      
            }
        });
       
       
       limpiar = new JButton();
       limpiar.setFont(new java.awt.Font("Arial", 5, 14));
       limpiar.setText("Vaciar");
       limpiar.setBounds(316, 250, 100, 30);
       getContentPane().add(limpiar);
       limpiar.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent evt) {
                limpiarActionPerformed(evt);    
            }
        });
       
       
       eliminar = new JButton();
       eliminar.setFont(new java.awt.Font("Arial", 5, 14));
       eliminar.setText("Eliminar");
       eliminar.setBounds(416, 250, 100, 30);
       getContentPane().add(eliminar);
       eliminar.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent evt) {
                eliminarActionPerformed(evt);                
            }
        });
       
       
       
       // Creamos un nuevo vector de "objectos" referenciado por data
       Object[][] data= new Object [][]{};
       
       // Creamos un vector de Strings referenciado por cabeceras
       String[] cabeceras = new String[]{
            "DNI", "Nombre", "Apellidos", "Edad", "Teléfono", "Profesión", 
           "Email", "Contraseña"
        };
       
       // Creamos la tabla
       jtblDatos = new JTable();
       // Añadimos a la tabla un escuchador de eventos de tipo MouseEvent.
       // Lo añadimos para que al dar click en uno de los objetos de la tabla,
       // se produzca lo que nosotros deseamos.
       jtblDatos.addMouseListener(new MouseAdapter()
        {

            public void mouseClicked(MouseEvent evt) {
                // Método que tratará el evento
                muestraDatosMouseClicked(evt);
            }
            /**
             * muestraDatosMouseClicked.
             * @param evt 
             */
            private void muestraDatosMouseClicked(MouseEvent evt){
                // Cuando se seleccione un objeto de la tabla, los campos y 
                // botones se deshabilitarán
                deshabilitar();
                // Pero luego habilitamos el botón nuevo
                nuevo.setEnabled(true);
                // Y el boton eliminar
                campoContraseña.setEnabled(true);
                
                // Referenciamos un valor int con el nombre fila.
                // Este valor será el índice que se obtendrá de la fila (ROW)
                // que nosotros SELECCIONEMOS con el ratón, trackpad...
                int fila = jtblDatos.getSelectedRow();
                // Si el valor de la fila es igual o mayor a 0...
                if (fila >=0) { 
                    // Añadiremos a cada campo correspondiente los valores 
                    // obtenidos de la fila seleccionada.
                    // Para ello invocamos al método getValueAt(fila seleccio-
                    // nada, y el valor de la cabecera en donde se encuentra el
                    // valor que queremos obtener. Tenemos que tener el detalle
                    // de convertir los valores recibidos a String para que se 
                    // puedan añadir los valores a los campos.
                    campoD.setText(jtblDatos.getValueAt(fila, 0).toString());
                    campoN.setText(jtblDatos.getValueAt(fila, 1).toString());
                    campoA.setText(jtblDatos.getValueAt(fila, 2).toString());
                    campoEdad.setText(jtblDatos.getValueAt(fila, 3).toString());
                    campoTel.setText(jtblDatos.getValueAt(fila, 4).toString());
                    prof.setSelectedItem(jtblDatos.getValueAt(fila, 
                            5).toString());
                    campoEmail.setText(jtblDatos.getValueAt(fila, 
                            6).toString());
                    campoContraseña.setText(jtblDatos.getValueAt(fila, 
                            7).toString());
                    campoContraseña.setEnabled(false);
                }
            }
        });
       
       // Creamos el Scroll para poder deslizarnos...
       scroll = new JScrollPane();
       //jtblDatos.addListSelectionListener();
        
    
       // Añadimos a la tabla el modelo... 
       // Recordemos: xd SetDefaultModel (designado al principio de la clase), 
       // ahora lo creamos dentro del método ""setModel", pasándole en su
       // constructor (por crearlo) el vector data(filas) y cabeceras(cabeceras
       // de las columnas de la tabla).
       jtblDatos.setModel(xd = new javax.swing.table.DefaultTableModel(data, 
               cabeceras){
           
            // Decimos el tipo de columna que será cada una de la tabla (8 
            // columnas).
            Class [] tipoColumn = { java.lang.String.class,
                                    java.lang.String.class,
                                    java.lang.String.class,
                                    java.lang.String.class,
                                    java.lang.String.class,
                                    java.lang.String.class,
                                    java.lang.String.class,
                                    java.lang.String.class};
            
            // Definimos las columnas para que no se puedan editar
            boolean[] editColum = { false, false, false, false, false, 
                false, false, false };
            
            /**
             * Método obtener columna por índice. 
             * @param indColum Nos retorna el tipo de Columna según el índice
             * que cada una tenga... como hay 8 columnas tendremos los índices
             * 0,1,2,3,4,5,6,7
             * @return 
             */
            public Class getColumnClass(int indColum){
                return tipoColumn[indColum];
            }
            
            /**
             * 
             * @param indFila
             * @param indColum
             * @return 
             */
            public boolean isCellEditable(int indFila, int indColum){
                return editColum[indColum];
                }
            });
       
            //((DefaultTableModel)jtblDatos.getModel()).addRow(data);
            
            // Creamos un vector de tipo int referenciado por "anchos". Vemos 
            // que hay 8 valores, los cuales son lo ancho que será cada una de 
            // las columnas según el orden establecido en la cabecera:
            // DNI(0)=58; NOMBRE(1)=100; APELLIDOS(2)=150; EDAD(3)=30;
            // TELEFONO(4)=70; PROFESION(5)=70; EMAIL(6)=160; CONTRASEÑA(7)=110
            int[] anchos = {58, 100, 150, 30, 70, 70, 160, 110};
            
            // INICIAMOS UN BUCLE, DE MANERA QUE DEL 0 PASARÁ AL 1, DEL 1 AL
            // 2 Y ASÍ SUCESIVAMENTE MIENTRAS SE CUMPLA LA CONDICION
            for(int i=0; i<jtblDatos.getColumnCount(); i++){
                // Para cada índice se obtiene el modelo de la columna de la 
                // tabla, la columna del índice y seguidamente se le pone el 
                // tamaño de ancho  definido en el vector de anchos a dicha 
                // columna. Por ello, hay que poner el tamaño de anchos tomando 
                // consciencia del orden de las columnas, si queremos que salga 
                // como deseamos, claro...
                jtblDatos.getColumnModel().getColumn(i).
                        setPreferredWidth(anchos[i]);
            }
            
            // Referenciamos nuestra cabecera de tabla por th
            JTableHeader th;
            // obtenemos la cabecera de la tabla
            th = jtblDatos.getTableHeader();
            // Ponemos, por cuestión de diseño, un tipo de letra y tamaño de 
            // letra a la cabecera de la tabla
            th.setFont(new Font("Arial", 1, 14));
       
      
    // Añadimos a la tabla un SCROLL para poder desplazarnos de arriba a bajo
    scroll.setViewportView(jtblDatos);
    scroll.setBounds(20, 300, 860, 270);
    jtblDatos.setFont(new Font("Arial", 5, 12));
    getContentPane().add(scroll);
    
    // BACKGROUND... creamos un objeto de la clase PanelImagen, pasándole en su
    // constructor un String, donde se localiza la imagen que deseamos cargar.
    PanelImagen p= new PanelImagen("/Imagenes/Splash.jpg");
        // Añadimos la imagen al panel
        getContentPane().add(p);
        // La situamos
        p.setBounds(0, 0, 900, 600);
    }
    
    /**
     * Método vaciar. Este método nos permitirá vaciar todos los campos 
     * rellenados para poder volver a rellenarlos con diferentes datos
     */
    public void vaciar(){
        campoN.setText("");
        campoA.setText("");
        campoD.setText("");
        campoTel.setText("");
        prof.setSelectedItem(null);
        campoContraseña.setText("");
        campoContraseña.setEnabled(false);
        campoEmail.setText("");
        campoEmail.setEnabled(false);
        campoEdad.setText(""); 
        campoN.requestFocus();
    } 
    
    /**
     * Método deshabilitar. Este método NO nos permitirá escribir sobre los
     * campos del formulario.
     */
    public void deshabilitar(){
        campoN.setText("");
        campoA.setText("");
        campoD.setText("");
        campoTel.setText("");
        prof.setSelectedItem(null);
        campoContraseña.setText("");
        campoEmail.setText("");
        campoEdad.setText(""); 
        campoN.setEnabled(false);
        campoA.setEnabled(false);
        campoD.setEnabled(false);
        campoTel.setEnabled(false);
        prof.setEnabled(false);
        campoContraseña.setEnabled(false);
        campoEmail.setEnabled(false);
        campoEdad.setEnabled(false);
        guardar.setEnabled(false);
        cancelar.setEnabled(false);
        limpiar.setEnabled(false);
        refresh.setEnabled(false);
    }
    
    /**
     * Método habilitar. Este método nos permitirá escribir sobre los campos
     * del formulario cuando necesitamos rellenarlos, o lo que es lo mismo,
     * cuando demos click al botón NUEVO y este trate dicho evento con este 
     * método para poder habilitar los campos y poder escribir en éstos.
     */
    public void habilitar(){
        campoN.setEnabled(true);
        campoA.setEnabled(true);
        campoD.setEnabled(true);
        campoTel.setEnabled(true);
        prof.setEnabled(true);
        campoContraseña.setEnabled(false);
        campoEmail.setEnabled(false);
        campoEdad.setEnabled(true);
        nuevo.setEnabled(false);
        eliminar.setEnabled(false);
        guardar.setEnabled(true);
        cancelar.setEnabled(true);
        limpiar.setEnabled(true);
        refresh.setEnabled(true);
        campoN.setText("");
        campoA.setText("");
        campoD.setText("");
        campoTel.setText("");
        prof.setSelectedItem(null);
        //campoContraseña.setText("");
        //campoEmail.setText("");
        campoEdad.setText("");
        campoEmail.setText("");
        campoContraseña.setText("");
        // Pasamos el foco al campo Nombre
        campoN.requestFocus();
    }
    
    
    /**
     * limpiarActionPerformed. Método asociado al evento del botón VACIAR.
     * Cuando ocurra el evento(click), este método tendrá lugar, vaciando todos  
     * los campos. Por ello hemos invocado dentro de este método  
     * al método vaciar();
     * @param evt 
     */
    private void limpiarActionPerformed(ActionEvent evt){
        // Método vaciar que tendrá lugar cuando ocurra el evento.
        vaciar();       
    }
    
    /**
     * nuevoActionPerformed. Método asociado al evento del botón NUEVO.
     * Cuando ocurra el evento(click), este método tendrá lugar, habilitando 
     * todos los campos para que puedan ser utilizados. Por ello hemos invocado
     * dentro de este método al método habiltiar();
     * @param evt 
     */ 
    private void nuevoActionPerformed(ActionEvent evt){
        // Método habilitar que tendrá lugar cuando ocurra el evento.
        habilitar();
    }
    
    /**
     * cancelarActionPerformed. Cuando se cancela la operación de ingresar un
     * trabajador.
     * @param evt 
     */
    private void cancelarActionPerformed(ActionEvent evt){
        // Invocamos al método deshabilitar
        deshabilitar();
        // Habilitamos los botones nuevo y eliminar para nuevas operaciones
        nuevo.setEnabled(true);
        eliminar.setEnabled(true);
    }
    
    /**
     * eliminarActionPerformed. Método que tratará el botón eliminar.
     * @param evt 
     */
     private void eliminarActionPerformed(ActionEvent evt){
         // Obtenemos el índice de la fila seleccionada
         int eliminar = jtblDatos.getSelectedRow();
         // Creamos un String que nos servirá más adelante para la sentencia
         // SQL. Como eliminaremos tuplas de la base de datos por DNI, 
         // obtendremos el valor del DNI de la fila seleccionada. Tenemos que
         // acordarnos de pasar a String el valor obtenido.
         String DNi = jtblDatos.getValueAt(eliminar, 0).toString();
         
         // Si la fila seleccionada es 0 (ninguna)
         if(eliminar<0){
             // Mostraremos un mensaje advirtiendo de que no ha seleccionado a 
             // ningún trabajador para poder eliminarlo
             miImagen = new ImageIcon(getClass().getResource("/icons/removeUser.png"));
             // Mostramos un Mensaje, donde le pasamos como parámetros el 
             // mensaje que le aparecerá a nuestro cliente, el título de la 
             // ventana de mensaje, el tipo de mensaje y el icono que hemos 
             // cargado para que aparezca.
             JOptionPane.showMessageDialog(null, "No ha seleccionado ningún "
                     + "trabajador.", "Error",JOptionPane.WARNING_MESSAGE,
                     miImagen);
        // En caso contrario...
        }else{
             // Mostramos una venana de confirmación mostrando un mensaje, con
             // el valor del nombre y apellidos de la fila seleccionada. Si la
             // confirmación es SI (valor 0), procederemos a eliminar el 
             // trabajador de la tabla y de la base de datos
             if(showConfirmDialog(this, "¿Seguro que quieres despedir al"+ 
                     " trabajador "+
                     jtblDatos.getValueAt(jtblDatos.getSelectedRow(), 1)+" "+
                     jtblDatos.getValueAt(jtblDatos.getSelectedRow(), 2)+
                     "?","Confirmar",0)==0){
                 // Eliminamos la fila de nuestro estilo de tabla
                 xd.removeRow(jtblDatos.getSelectedRow());
                 //jtblDatos.getComponent(0);
            
                 
            try{
                // CARGAMOS EL DRIVER
                Class.forName("org.apache.derby.jdbc.EmbeddedDriver");      
            }catch (ClassNotFoundException e){ 
                System.out.println(e);
            } 
            
            try{
                // ESTABLECEMOS LA CONEXIÓN CON LA BASE DE DATOS, PASÁNDOLE
                // LA RUTA, EL NOMBRE DE LA BASE Y EL USUARIO Y CONTRASEÑA
                Connection mibda = 
                DriverManager.getConnection("jdbc:derby://localhost:1527/"
                    + "EmpresaBiomed","LeanMaria", "leccaclua");
            
                // CONSTRUIMOS LA SENTENCIA SQL DE BORRADO. BORRARÁ POR EL DNI
                // QUE YA HEMOS OBTENIDO DE LA FILA SELECCIONADA
                PreparedStatement ordenSQL = mibda.prepareStatement("DELETE "
                        + "FROM biomed.trabajadores WHERE DNI='"+DNi+"'");
                // EJECUTAMOS LA ORDEN SQL
                ordenSQL.executeUpdate();
                // DESHABILITAMOS LOS CAMPOS PARA TENER LAS FUNCIONES DE NUEVO
                // U ELIMINAR
                deshabilitar();
            
            // Capturamos los errores    
            }catch (SQLException e){
                System.err.println(e);
            }      
         }}
    }

    
    
  
    /**
     * campoNTeclaActioPerformed. Método que trata el evento de teclado.
     * @param evt 
     */
    private void campoNTeclaActionPerformed(java.awt.event.KeyEvent evt){   
        // Si el código obtenido de la tecla presionada es igual al TAB,
        // el foco pasará al campo que deseamos (en nuestro caso el de debajo)
        if (evt.getKeyCode() == KeyEvent.VK_TAB) {
            campoA.requestFocus();
        }
        
        // La misma estructura de código, pero para cuando la tecla sea ENTER
        if (evt.getKeyCode() == KeyEvent.VK_ENTER){
            campoA.requestFocus();           
        }
    }
    
    // ** DE FORMA ANÁLOGA PARA LOS DEMÁS MÉTODOS QUE TRATAN EVENTOS DE
    // TIPO KEYEVENT ** //
    
    
    /**
     * campoATeclaActionPerformed.
     * @param evt 
     */
    private void campoATeclaActionPerformed(java.awt.event.KeyEvent evt){
        if(evt.getKeyCode() == KeyEvent.VK_TAB){
            campoD.requestFocus();
        }
        
        if (evt.getKeyCode() == KeyEvent.VK_ENTER){
            campoD.requestFocus();           
        }    
    }
    
    
    /**
     * campoDTeclaActionPerformed.
     * @param evt 
     */
    private void campoDTeclaActionPerformed(java.awt.event.KeyEvent evt){
        if(evt.getKeyCode() == KeyEvent.VK_TAB){
            campoEdad.requestFocus();
        }
        if (evt.getKeyCode() == KeyEvent.VK_ENTER){
            campoEdad.requestFocus();           
        } 
    } 
    
    
    /**
     * campoEdadTeclaActionPerformed.
     * @param evt 
     */
    private void campoEdadTeclaActionPerformed(java.awt.event.KeyEvent evt){
        if(evt.getKeyCode() == KeyEvent.VK_TAB){
            campoTel.requestFocus();
        }
        if (evt.getKeyCode() == KeyEvent.VK_ENTER){
            campoTel.requestFocus();           
        } 
    }
    
    
    /**
     * campoTelTeclaActionPerformed.
     * @param evt 
     */
    private void campoTelTeclaActionPerformed(java.awt.event.KeyEvent evt){
        if(evt.getKeyCode() == KeyEvent.VK_TAB){
            prof.requestFocus();
        }
        if (evt.getKeyCode() == KeyEvent.VK_ENTER){
            prof.requestFocus();           
        } 
    }
    
    /**
     * profTeclaActionPerformed.
     * @param evt 
     */
    private void profTeclaActionPerformed(java.awt.event.KeyEvent evt){
        if(evt.getKeyCode() == KeyEvent.VK_TAB){
            guardar.requestFocus();
        }
        else if(evt.getKeyCode() == KeyEvent.VK_ENTER){
            guardar.requestFocus();
        }
    }
    
    
    /**
     * guardarTeclaActionPerformed.
     * @param evt 
     */
    private void guardarTeclaActionPerformed(java.awt.event.KeyEvent evt){
        if(evt.getKeyCode() == KeyEvent.VK_ENTER){
            campoN.requestFocus();
        }
        if(evt.getKeyCode() == KeyEvent.VK_TAB){
            campoN.requestFocus();
        }
    }
    
    /**
     * campoContraseñaTeclaActionPerformed.
     * @param evt 
     */
    private void campoContraseñaTeclaActionPerformed(java.awt.event.KeyEvent 
            evt){
        if(evt.getKeyCode() == KeyEvent.VK_TAB){
            guardar.requestFocus();
        }
        if (evt.getKeyCode() == KeyEvent.VK_ENTER){
            guardar.requestFocus();
        }
    }
    
    
    /**
     * campoNDebeEstarLleno.
     */
    private void campoNDebeEstarLleno(){
        
        // Si la longitud del campo Nombre es igual a 0
        if(campoN.getText().length() == 0){
            
            // Ponemos a null el Item seleccionado
            prof.setSelectedItem(null);
            
            // Ponemos el email a null
            campoEmail.setText(null);
            
            // Ponemos la contraseña a null
            campoContraseña.setText(null);
            
            // Prohibimos el poder escribir en el campo Email
            campoEmail.setEditable(false);
            
            // Prohibimos el poder escribir en el campo Contraseña
            campoContraseña.setEditable(false);
            miImagen = new ImageIcon(getClass().getResource("/icons/block.png"));
            // Y mostramos el mensaje de error al usuario para que le obliguemos
            // a rellenar el campo Nombre si quiere generar un email...
            JOptionPane.showMessageDialog(null, "Debe rellenar el campo nombre"+
                    " para generar el email.","Error", 
                    JOptionPane.WARNING_MESSAGE, miImagen);                    
        }
    }
    
    /**
     * campoADebeEstarLleno. Análogamente al método anterior para el campo 
     * apellidos.
     */
    private void campoADebeEstarLleno(){
        // Si la longitud del campo Apellidos es igual a 0
        if(campoA.getText().length() == 0){
            prof.setSelectedItem(null);
            campoEmail.setText(null);
            campoContraseña.setText(null);
            campoEmail.setEditable(false);
            campoContraseña.setEditable(false);
            miImagen = new ImageIcon(getClass().getResource("/icons/block.png"));
            // Y mostramos el mensaje de error al usuario para que le obliguemos
            // a rellenar el campo Apellidos si quiere generar un email...
            JOptionPane.showMessageDialog(null, "Debe rellenar el campo"+
                    " apellidos para generar el email.","Error", 
                    JOptionPane.WARNING_MESSAGE, miImagen);       
        }
    }
    
    
    
    /**
     * Método Item. Trata el evento producido por la selección de un Item.
     * @param evt 
     */
    private void profItemActionPerformed(java.awt.event.ItemEvent evt){
        
        // Designamos un índice obtenido mediante el método getSelectedIndex
        // del componente JComboBox referenciado por prof. Cada Item tendrá
        // un índice (0,1,2,3)=4 componentes. El 0 será el médico, el 1 será
        // el investigador, el 2 será el administrador, y finalmente, el 3 
        // será el gerente.
        int i = prof.getSelectedIndex();
        
        // Si el elemento seleccionado siendo su índice obtenido mediante
        // getSelectedIndex es 0, es decir, el del médico...
        if(i == 0){
            
            // Si el campo Nombre está vacio...
            campoNDebeEstarLleno(); // ver método
            
            // Si el campo Apellidos está vacio...
            campoADebeEstarLleno(); // ver método
            
            // Si el campoN y campoA NO están vacíos...
            if (campoN.getText().length()!= 0 && campoA.getText().length()!=0){
                // Habilitamos el campo email
                //campoEmail.setEnabled(true);
                // Habilitamos el campo contraseña
                campoContraseña.setEnabled(true);
                // Creamos un String obteniendo los tres primeros carácteres
                // del campoN
                String primeraParte = campoN.getText().substring(0,3);
                // Creamos un String obteniendo los tres primeros carácteres del
                // campoA
                String segundaParte = campoA.getText().substring(0,3);
                // En el campoEmail pondremos los String creados CONCATENADOS,
                // he invocando al método toLowerCase(); para que las mayúsculas
                // se conviertan en minúsculas. Por otra parte, asociamos a
                // el string concatenado el ALIAS de EMAIL. Como 0 es Médico,
                // pondremos un @med.com (MED = DE MÉDICO).
                campoEmail.setText(primeraParte.toLowerCase()+"."+
                    segundaParte.toLowerCase()+"@med.com");
                //campoEmail.transferFocus();
                // Pasamos el foco al campo contraseña para poder escribir la
                // que deseemos.
                campoContraseña.requestFocus(); 
            }          
        }
        
        // ** DE FORMA ANÁLOGA PARA LOS RESTANTES ÍTEMS DEL JCOMBOBOX ** //

        if(i==1){
            
            campoNDebeEstarLleno();
            campoADebeEstarLleno();
            
            if (campoN.getText().length()!= 0 && campoA.getText().length()!=0){
                //campoEmail.setEnabled(true);
                campoContraseña.setEnabled(true);
                String primeraParte = campoN.getText().substring(0,3);
                String segundaParte = campoA.getText().substring(0,3);
                campoEmail.setText(primeraParte.toLowerCase()+"."+
                    segundaParte.toLowerCase()+"@invest.com");
                //campoEmail.transferFocus();
                campoContraseña.requestFocus();
            }       
        }
        
        if(i==2){
            campoNDebeEstarLleno();
            campoADebeEstarLleno();
            //campoNyADebenEstarLlenos();
            
            if (campoN.getText().length()!= 0 && campoA.getText().length()!=0){
                //campoEmail.setEnabled(true);
                campoContraseña.setEnabled(true);
                String primeraParte = campoN.getText().substring(0,3);
                String segundaParte = campoA.getText().substring(0,3);
                campoEmail.setText(primeraParte.toLowerCase()+"."+
                    segundaParte.toLowerCase()+"@admin.com");
                campoContraseña.requestFocus();
            }
        }
        
        if(i==3){
            campoNDebeEstarLleno();
            campoADebeEstarLleno();
            //campoNyADebenEstarLlenos();
            
            if (campoN.getText().length()!= 0 && campoA.getText().length()!=0){
                //campoEmail.setEnabled(true);
                campoContraseña.setEnabled(true);
                String primeraParte = campoN.getText().substring(0,3);
                String segundaParte = campoA.getText().substring(0,3);
                campoEmail.setText(primeraParte.toLowerCase()+"."+
                    segundaParte.toLowerCase()+"@gerente.com");
                campoContraseña.requestFocus();
            }
        }
    }
    
    /**
     * refreshActionPerformed. Método que actuará si se modifica, 
     * posteriormente de seleccionar la profesión, el nombre y apellidos 
     * actualizando el email
     * @param evt 
     */
    private void refreshActionPerformed(ActionEvent evt){
        
        int i = prof.getSelectedIndex();
        
        if(i==0){   
            String primeraParte = campoN.getText().substring(0,3);
            String segundaParte = campoA.getText().substring(0,3);
            campoEmail.setText(primeraParte.toLowerCase()+"."+
                    segundaParte.toLowerCase()+"@med.com");
            campoEmail.setEnabled(false);
            // Pasamos directamente el foco al botón guardar
            guardar.requestFocus(); 
        }
            
                                  
        if(i==1){
            String primeraParte = campoN.getText().substring(0,3);
            String segundaParte = campoA.getText().substring(0,3);
            campoEmail.setText(primeraParte.toLowerCase()+"."+
                    segundaParte.toLowerCase()+"@invest.com");
            campoEmail.setEnabled(false);          
            guardar.requestFocus();
        }
        
        if(i==2){
            String primeraParte = campoN.getText().substring(0,3);
            String segundaParte = campoA.getText().substring(0,3);
            campoEmail.setText(primeraParte.toLowerCase()+"."+
                    segundaParte.toLowerCase()+"@admin.com");
            campoEmail.setEnabled(false);
            guardar.requestFocus();
        }
        
        if(i==3){
            String primeraParte = campoN.getText().substring(0,3);
            String segundaParte = campoA.getText().substring(0,3);
            campoEmail.setText(primeraParte.toLowerCase()+"."+
                    segundaParte.toLowerCase()+"@gerente.com");
            campoEmail.setEnabled(false);
            guardar.requestFocus();
        }
    }
    
        
    /**
     * guardarActionPerformed. Método guardar.
     * @param evt 
     */
    private void  guardarActionPerformed(java.awt.event.ActionEvent evt) {
        
        // Creamos Strings cuyos valores serán los obtenidos de los campos
        String DNI = campoD.getText();
        String Nombre = campoN.getText();
        String Apellidos = campoA.getText();
        String Edad = campoEdad.getText();
        String Telefono = campoTel.getText();
        String Prof= prof.getSelectedItem().toString();
        String EMAIL = campoEmail.getText();
        String Contraseña = campoContraseña.getText();
        // Creamos un vector de datos en el orden en que se visualizarán
        // en la tabla
        String datos[]={DNI, Nombre, Apellidos, Edad, Telefono, Prof, EMAIL, 
            Contraseña};
        // Añadimos la fila con los valores a las respectivas cabeceras de la 
        // nueva fila de la tabla
        xd.addRow(datos);
        
        // Además, haremos el ingreso en la base de datos. Cargamos el DRIVER.
        try{
            Class.forName("org.apache.derby.jdbc.EmbeddedDriver");
        }catch (ClassNotFoundException e){
            System.out.println(e);
        }
        
        // CREAMOS LA CONEXIÓN CON LA BASE DE DATOS
        try{
            Connection mibda = 
            DriverManager.getConnection("jdbc:derby://localhost:1527/"
                    + "EmpresaBiomed", "LeanMaria", "leccaclua");
            
            // CREAMOS LA SENTENCIA SQL
            PreparedStatement pst = mibda.prepareStatement("INSERT INTO "
                    + "biomed.trabajadores (DNI, Nombre, Apellidos, Edad, "
                    + "Telefono, Profesion, Email, Contraseña) VALUES (?, ?, "
                    + "?, ?, ?, ?, ?, ?)");    
            
            // INGRESAMOS EN NUESTRA TABLA DE LA BASE DE DATOS LOS VALORES!
            // VEMOS QUE LA PRIMERA CABECERA DE LA TABLA DE LA BD EMPIEZA POR 
            // ÍNDICE 1, A DIFERENCIA DE LA JTABLE, EN DONDE LA PRIMERA CABECE-
            // RA TIENE ÍNDICE 1
            pst.setString(1, DNI);
            pst.setString(2, Nombre);
            pst.setString(3, Apellidos);
            pst.setString(4, Edad);
            pst.setString(5, Telefono);
            pst.setString(6, Prof);
            pst.setString(7, EMAIL);
            pst.setString(8, Contraseña);
            // EJECUTAMOS LA ORDEN
            pst.executeUpdate();
            
            // SI TODO HA TENIDO ÉXITO, AVISAREMOS AL CLIENTE QUE HA INGRESADO
            // AL TRABAJADOR CON ÉXITO
            miImagen = new ImageIcon(getClass().getResource("/icons/accept.png"));
            JOptionPane.showMessageDialog(null, "Ha ingresado al tabajador "
                    +campoN.getText()+" "+campoA.getText()+" con éxito.", null,
                    JOptionPane.WARNING_MESSAGE, miImagen);
            
        // SI NO HA SIDO INGRESADO CON ÉXITO, SE PRODUCIRÁ LA EXCEPCIÓN
        }catch (SQLException e){
            System.err.println(e);
        }              
    }
    
    /**
     * visualizar. Método para que carguen todos los datos de la BD a nuestra
     * JTABLE.
     */
    public void visualizar(){
        // Cargamos el driver
        try{
            Class.forName("org.apache.derby.jdbc.EmbeddedDriver");
        }catch (ClassNotFoundException e){
            System.out.println(e);
        }
        
        // Realizamos la conexión con la base de datos
        try{
            Connection mibda = 
            DriverManager.getConnection("jdbc:derby://localhost:1527/"
                    + "EmpresaBiomed", "LeanMaria", "leccaclua");
            
            // Preparamos la sentencia SQL
            java.sql.Statement ordenSQL = mibda.createStatement();
            ResultSet rs = ordenSQL.executeQuery("SELECT * "
                    + "FROM biomed.trabajadores");
            
            // Mientras hayan datos por cargar...
            while (rs.next()){  
               // Almacenamos los datos obtenidos en Strings
               String d = rs.getString("DNI");
               String n = rs.getString("Nombre");
               String a = rs.getString("Apellidos");
               String e = rs.getString("Edad");
               String t = rs.getString("Telefono");
               String p = rs.getString("Profesion");
               String EM = rs.getString("Email");
               String c = rs.getString("Contraseña");
               // Creamos un vector de String en el orden lógico respecto a la
               // cabecera, para que los datos que se visualicen correspondan
               // con sus atributos
               String datos[]={d, n, a, e, t, p, EM, c};
               // Añadimos a nuestro modelo de tabla los datos
               xd.addRow(datos);
            }
            
        // En caso de error, se captura     
        }catch (SQLException e){
            System.err.println(e);
        }              
    }
    
    
    public static void main (String [] args){
        addTb trabajadores = new addTb();
    }  
}
